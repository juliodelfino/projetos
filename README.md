**Projetos Publicos de Julio Delfino**

*Qualquer duvida veja se tem um tutorial disponivel em https://www.youtube.com/channel/UCaLwTigS0QxRjYDAe_rNcZA Ou entre em contato:*

*Whatsapp* (17) 9 8134-1148
*E-mail* jcdsjuliocesardelfino@gmail.com
*Instagram* @jcdelmine

## Baixar Repositório
1. Instale o git
2. clique com o botão direito na pasta que quer baixar
3. clique em Git Bash
4. insira git clone https://juliodelfino@bitbucket.org/juliodelfino/projetos-publicos.git Projetos_Publucos_JulioDelfino
5. Para atualizar para uma nova versão faça até o passo 3 e digite git pull origin master
