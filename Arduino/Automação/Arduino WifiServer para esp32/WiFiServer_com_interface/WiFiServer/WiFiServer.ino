/*
  Arduino WifiServer com Interface na OLED
  Feito Por Julio Cesar Delfino da Silva / instagram @jcdelmine
*/

//Informações da sua rede Wifi
const char* ssid1     = "primeirarede"; //Nome da sua Primeira Rede
const char* password1 = "primeirarede"; //Senha da sua Primeira Rede

//Se não tiver mais de uma rede deixe como esta
const char* ssid2     = "segundarede"; //Nome da sua segunda Rede
const char* password2 = "segundarede"; //Senha da sua segunda Rede

const char* ssid3     = "terceirarede"; //Nome da sua terceira Rede
const char* password3 = "terceirarede"; //Senha da sua terceira Rede
//-----------------------------

//informações do servidor ap
const char* apssid = "Arduino";
const char* appassword = "arduinoesp32";
//quatidade de clientes conectados simultaniamente ao servidor ap
int qtdcliente = 15;
//--------------------------

//Incluindo as bibliotecas
#include <ETH.h>
#include <WiFi.h>
#include <WiFiAP.h>
#include <WiFiClient.h>
#include <WiFiGeneric.h>
#include <WiFiMulti.h>
#include <WiFiScan.h>
#include <WiFiServer.h>
#include <WiFiSTA.h>
#include <WiFiType.h>
#include <WiFiUdp.h>
#include <Wire.h> 
#include <SSD1306.h>

//pinos a serem ligados
int pin1 = 22;
int pin2 = 23;
int pin3 = 12;
int pin4 = 17;
int pin5 = 13;
//---------------------

//Variaveis internas (Não Mecher)
const char* ssid;
const char* password;
int status1;
int status2;
int status3;
int status4;
int status5;
int count;
int servidor = 0;
int tentativa = 0;
int html=1;
int menu=0;
int x=21;
int y=0;
int animacao=0;
String pagina="";
String type="";
int lista=0;
int reiniciar=0;
//---------------------------------

//Incluindo os arquivos de imagem
#include "menu.h"
#include "sem_conexao.h"
#include "conexao1.h"
#include "conexao2.h"
#include "conexao3.h"
#include "hotspot.h"
#include "iniciando.h"
#include "menu_lista.h"
#include "wifi1.h"
#include "wifi2.h"
#include "wifi3.h"
#include "wifi4.h"
#include "lista1.h"
#include "lista2.h"
#include "lista3.h"
#include "lista4.h"
#include "pino1.h"
#include "pino2.h"
#include "pino3.h"
#include "pino4.h"
#include "pino5.h"

//definições de posição da imagem
#define posX 21
#define posY 0

//botões
int btn1=36;
int btn2=37;
int btn3=38;
int btn4=39;
//-----------

SSD1306 display(0x3c, 4, 15); //Cria e ajusta o Objeto display

WiFiServer server(80); //Inicia o servidor com a porta especificada pode colocar qualquer uma (a porta 80 é a porta padrão do navegador)

void setup(){ // sera executado apenas algumas vezes
  Serial.begin(115200);
  //O estado do GPIO16 é utilizado para controlar o display OLED
  pinMode(16, OUTPUT);
  digitalWrite(reiniciar, OUTPUT);
  pinMode(btn1, INPUT);
  pinMode(btn2, INPUT);
  pinMode(btn3, INPUT);
  pinMode(btn4, INPUT);
  //Reseta as configurações do display OLED
  digitalWrite(16, LOW);
  digitalWrite(reiniciar, HIGH);
  //Para o OLED permanecer ligado, o GPIO16 deve permanecer HIGH
  //Deve estar em HIGH antes de chamar o display.init() e fazer as demais configurações,
  //não inverta a ordem
  digitalWrite(16, HIGH);

  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_16);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.flipScreenVertically(); //inverte verticalmente a tela (opcional)
  delay(500);
  display.clear(); //limpa tela
  //carrega para o buffer o frame 1
  //usando as posições iniciais posX e posY
  //informa o tamanho da imagem com iniciando_width e iniciando_height
  //informa o nome da matriz que contem os bits da imagem, no caso iniciando_bits
  display.drawXbm(posX, posY, iniciando_width, iniciando_height, iniciando_bits); 
  //mostra o buffer no display
  display.display();
  pagina="menu";
  type="menu";
  delay(1000);

  Serial.begin(115200);   //Inicia a porta serial
  Serial.println();
  Serial.println();
  Serial.println();

  //Define o pino para OUTPUT
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  pinMode(pin4, OUTPUT);
  pinMode(pin5, OUTPUT);
  //-------------------------

  //Desliga todos os pinos
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, LOW);
  digitalWrite(pin4, LOW);
  digitalWrite(pin5, LOW);
  //----------------------
  
  WiFi.disconnect();
  wifi(); //chama a função wifi
  delay(100); //espera 100 milisegundos para não dar erro

}

//voids de conexão

void wifi(){
  if(digitalRead(btn3)==0){
    if (WiFi.status() != WL_CONNECTED) {
      //faz uma animação do simbolo do wifi
      animacao=1;
        display.clear();
        display.drawXbm(posX, posY, wifi1_width, wifi1_height, wifi1_bits); 
        display.display();
        delay(500);
        display.clear();
        display.drawXbm(posX, posY, wifi2_width, wifi2_height, wifi2_bits); 
        display.display();
        delay(500);
        display.clear();
        display.drawXbm(posX, posY, wifi3_width, wifi3_height, wifi3_bits); 
        display.display();
        delay(500);
        display.clear();
        display.drawXbm(posX, posY, wifi4_width, wifi4_height, wifi4_bits); 
        display.display();
        if(digitalRead(btn3)==1){
          loop();
        }
        delay(1000);
        display.clear();
        display.drawXbm(posX, posY, wifi1_width, wifi1_height, wifi1_bits); 
        display.display();
        delay(500);
        display.clear();
        display.drawXbm(posX, posY, wifi2_width, wifi2_height, wifi2_bits); 
        display.display();
        delay(500);
        display.clear();
        display.drawXbm(posX, posY, wifi3_width, wifi3_height, wifi3_bits); 
        display.display();
        delay(500);
        display.clear();
        display.drawXbm(posX, posY, wifi4_width, wifi4_height, wifi4_bits); 
        display.display();
        if(digitalRead(btn3)==1){
          loop();
        }
        delay(1000);
        display.clear();
        display.drawXbm(posX, posY, wifi1_width, wifi1_height, wifi1_bits); 
        display.display();
        delay(500);
        display.clear();
        display.drawXbm(posX, posY, wifi2_width, wifi2_height, wifi2_bits); 
        display.display();
        delay(500);
        display.clear();
        display.drawXbm(posX, posY, wifi3_width, wifi3_height, wifi3_bits); 
        display.display();
        delay(500);
        display.clear();
        display.drawXbm(posX, posY, wifi4_width, wifi4_height, wifi4_bits); 
        display.display();
        if(digitalRead(btn3)==1){
          loop();
        }
        delay(1000);
        pagina="menu";
        type="menu";
        //------------------------------------
    if (tentativa == 0) { //tenta a primeira rede
      display.clear();
      display.drawXbm(posX, posY, sem_conexao_width, sem_conexao_height, sem_conexao_bits); 
      display.display();
      delay(1000);
      tentativa = 1;
      conecta1(); //define as informaçoes da primeira rede
      conectar(); //tenta conectar na primeira rede
    } else if (tentativa == 1) { //tenta a segunda rede
      display.clear();
      display.drawXbm(posX, posY, sem_conexao_width, sem_conexao_height, sem_conexao_bits); 
      display.display();
      delay(1000);
      tentativa = 2;
      conecta2(); //define as informaçoes da segunda rede
      conectar(); //tenta conectar na segunda rede
    } else if (tentativa == 2) { //tenta a terceira rede
      display.clear();
      display.drawXbm(posX, posY, sem_conexao_width, sem_conexao_height, sem_conexao_bits); 
      display.display();
      delay(1000);
      tentativa = 3;
      conecta3(); //define as informaçoes da terceira rede
      conectar(); //tenta conectar na terceira rede
    } else if ((tentativa == 3) && (WiFi.status() != WL_CONNECTED) && (servidor==0)) { //Se não tem nenhuma rede disponivel cria um hotspot
      servidorap();
    }
  }
    if(html==1){
      html=0;
      server.begin();  //Inicia o servidor HTML na sua rede ou no hotspot criado
      Serial.print("Servidor HTML iniciado");
    }
    if((WiFi.status() == WL_CONNECTED) || (servidor==1)){
      loop();
    }
    delay(300);
  }else{
    delay(300);
    servidorap();
  }
}
//voids de conexão
void conecta1() {
  ssid = ssid1; //define o nome da primera rede
  password = password1; //define a senha da primera rede
}

void conecta2() {
  ssid = ssid2; //define o nome da primera rede
  password = password2; //define a senha da primera rede
  Serial.print("Mudando para: ");
  Serial.println(ssid);
}

void conecta3() {
  ssid = ssid3; //define o nome da primera rede
  password = password3; //define a senha da primera rede
  Serial.print("Mudando para: ");
  Serial.println(ssid);
}

void conectar() {
  // Inicia a conexão com a sua rede
  //Imprime na serial
  Serial.println();
  Serial.print("Conectando em: ");
  Serial.println(ssid);
  display.println("Conectando em: ");
  display.println(ssid);
  Serial.println();
  
  //Começa a conexão
  WiFi.begin(ssid, password);
  count = 0;
  while ((WiFi.status() != WL_CONNECTED) && (count <= 6)) { //loop de conexão
    delay(125);
    count++;
  }
  count=0;
  if (WiFi.status() != WL_CONNECTED) {
  //se não deu certo imprime na serial
  Serial.print("Sem conexão com: "); Serial.println(ssid);
  }
  //Se a conexão deu certo mostra o ip que pegou
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("");
    Serial.print("WiFi Conectado em: ");
    Serial.println(ssid);
    Serial.println("Endereço IP (para a conexão): ");
    Serial.println(WiFi.localIP()); //Imprime o ip que você devera conectar
    Serial.println("");
  }
//-----------------------------------------------

//avisa quantas tentativas e qual conexão deu certo
if(tentativa==1){
  Serial.print("foi executada apenas: ");
  Serial.print(tentativa);
  Serial.println(" tentativa");
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("E a primeira conexão deu certo");
     display.clear();
     display.drawXbm(posX, posY, conexao1_width, conexao1_height, conexao1_bits); 
     display.display();
    Serial.println();
    pagina="menu";
    type="menu";
  }
}else if(tentativa==2){
  Serial.print("foram executadas: ");
  Serial.print(tentativa);
  Serial.println(" tentativas");
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("E a segunda conexão deu certo");
    display.clear();
    display.drawXbm(posX, posY, conexao2_width, conexao2_height, conexao2_bits); 
    display.display();
    Serial.println();
    pagina="menu";
    type="menu";
  }
}else if(tentativa==3){
  Serial.print("todas as: ");
  Serial.print(tentativa);
  Serial.println(" tentativas foram executadas");
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("E a terceira conexão deu certo");
    display.clear();
    display.drawXbm(posX, posY, conexao3_width, conexao3_height, conexao3_bits); 
    display.display();
    Serial.println();
    pagina="menu";
    type="menu";
  }
  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("E nenhuma delas deu certo");
    Serial.println();
  }
}
  
  servidor=0; //se não tem servidor define que ainda pode criar se precisar
  delay(100);
  if(WiFi.status() != WL_CONNECTED){
  wifi();
  }
}

void servidorap(){ //cria o propio servidor caso não encontre nenhuma rede
      Serial.print("Criando Hotspot");
      Serial.println("");
      display.print("Criando Hotspot");
      display.println("");
      WiFi.softAP(apssid, appassword);
      IPAddress ip(192, 168, 4, 1);
      IPAddress gateway(192, 168, 4, 1);
      IPAddress subnet(255, 255, 255, 0);
      WiFi.softAPConfig(ip, gateway, subnet); //configura o servidor
      Serial.print("Servidor Criado: (Ip de conexão: "); Serial.print(gateway); Serial.println(")");
      Serial.print("ssid: "); Serial.println(apssid);
      Serial.print("Senha: "); Serial.println(appassword);
      Serial.println();
      display.print("Servidor Criado: (Ip de conexão: "); display.print(gateway); Serial.println(")");
      display.print("ssid: "); display.println(apssid);
      display.print("Senha: "); display.println(appassword);
      display.println();
      servidor = 1; //se criou o servidor define para não ser criado novamente
      display.clear();
      display.drawXbm(posX, posY, hotspot_width, hotspot_height, hotspot_bits); 
      display.display();
      pagina="menu";
      type="menu";
      loop();
}
//------------------------------------------------------------

void loop(){
  delay(100);
   if(tentativa != 0){
    tentativa=0;
  }
  if((WiFi.status() != WL_CONNECTED) && (servidor==0)) { //se a conexão caiu conecta novamente ou cria o hotspot
    wifi();
  }

  WiFiClient client = server.available();  //Verifica se algum cliente conectou

  if (client) {                                 // Se achou o cliente
    String linhaatual = "";                     // Define a string para enviar os dados do HTML
    while (client.connected()) {                // Loop para atender o cliente
      if (client.available()) {                 // Verifica se o cliente ainda ta conectado
        char c = client.read();                 // Define a variavel que vai receber os dados do Cliente pelo HTML
        if (c == '\n') {                        // Verifica se a variavel é uma nova linha

          // Se a variavel é uma linha em branco termina o requerimento do Cliente HTTP
          if (linhaatual.length() == 0) {
            //O HTTP Começa com um codigo de resposta (Ex. HTTP/1.1 200 OK)
            //Se quiser mude o HTML somente depois do print em branco
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println(); //Mude depois daqui

            //HTML Personalizado

            client.println("<head><title>Automacao Arduino</title></head>");
            client.println("<body style='background-color: rgb(0, 128, 192);'>");
            client.println("<div style='width: 720px;padding-right: 60px;padding-left: 60px;margin-right: auto;margin-left: auto;background-color: rgb(170, 255, 255);'>");
            client.println("<div align='center' style='padding-top: 50px;'><a href=\"/ligar\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>LIGAR TUDO</h1></div></a></div><table width='100%'>");
            client.println("<tr><td><div align='center'><div style='background-color: rgb(115, 230, 0);'><h1>1</h1></div></div></td>");
            client.println("<td><div align='center'><div style='background-color: rgb(115, 230, 0);'><h1>2</h1></div></div></td>");
            client.println("<td><div align='center'><div style='background-color: rgb(115, 230, 0);'><h1>3</h1></div></div></td>");
            client.println("<td><div align='center'><div style='background-color: rgb(115, 230, 0);'><h1>4</h1></div></div></td>");
            client.println("<td><div align='center'><div style='background-color: rgb(115, 230, 0);'><h1>5</h1></div></div></td></tr>");
            client.println("<tr><td><div align='center'><a href=\"/on1\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>ON</h1></div></a></div></td>");
            client.println("<td><div align='center'><a href=\"/on2\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>ON</h1></div></a></div></td>");
            client.println("<td><div align='center'><a href=\"/on3\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>ON</h1></div></a></div></td>");
            client.println("<td><div align='center'><a href=\"/on4\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>ON</h1></div></a></div></td>");
            client.println("<td><div align='center'><a href=\"/on5\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>ON</h1></div></a></div></td></tr>");
            client.println("<tr><td><div align='center'><a href=\"/off1\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>OFF</h1></div></a></div></td>");
            client.println("<td><div align='center'><a href=\"/off2\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>OFF</h1></div></a></div></td>");
            client.println("<td><div align='center'><a href=\"/off3\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>OFF</h1></div></a></div></td>");
            client.println("<td><div align='center'><a href=\"/off4\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>OFF</h1></div></a></div></td>");
            client.println("<td><div align='center'><a href=\"/off5\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>OFF</h1></div></a></div></td></tr>");
            client.println("</table><div align='center' style='padding-bottom: 10px'><a href=\"/desligar\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>DESLIGAR TUDO</h1></div></a></div>");
            client.println("<div align='center' style='padding-bottom: 50px'><a href=\"/wifi\"  style='text-decoration-line:none; color: black; '><div style='background-color: rgb(115, 230, 0);'><h1>CONECTAR NO WIFI</h1></div></a></div></div></body>");

            //---------------------

            //A resposta HTTP termina com um print em branco
            client.println();
            //Para o loop´do wihle
            break;
          } else {    //Se a variavel não for uma linha vazia limpa ela para receber mais uma linha
            linhaatual = "";
          }
        } else if (c != '\r') {
          linhaatual += c;      // Adicona um caracter de retorno no final da linha atual
        }

        // Checa o que o HTML retornou para o arduino
        //se clicou no WiFi
        if (linhaatual.endsWith("GET /wifi")) {
          Serial.println("Pediu para mudar a conexão para o wifi");
          if(WiFi.status() == WL_CONNECTED){
          Serial.print("Ja está conectado ao wifi: ");
          Serial.println(ssid);
          }else{
          tentativa=0;
          WiFi.softAPdisconnect(true); //desconecta o servidor
          servidor=0;
          display.println("mudando para o wifi");
          Serial.println();
          wifi();
          }
        }
        
        //Se for /ligar liga tudo
        if (linhaatual.endsWith("GET /ligar")) {
          digitalWrite(pin1, HIGH);
          digitalWrite(pin2, HIGH);
          digitalWrite(pin3, HIGH);
          digitalWrite(pin4, HIGH);
          digitalWrite(pin5, HIGH);
          status1=1;
          status2=1;
          status3=1;
          status4=1;
          status5=1;
          Serial.println("Ligou todos os pinos");
        }
        //----------------------

        //Se for /desligar desliga tudo
        if (linhaatual.endsWith("GET /desligar")) {
          digitalWrite(pin1, LOW);                // GET /Desligar turns the pin off
          digitalWrite(pin2, LOW);
          digitalWrite(pin3, LOW);
          digitalWrite(pin4, LOW);
          digitalWrite(pin5, LOW);
          status1=0;
          status2=0;
          status3=0;
          status4=0;
          status5=0;
          Serial.println("Desligou todos os pinos");
        }
        //------------------------------

        //Se for um pino começando com on liga ele
        if (linhaatual.endsWith("GET /on1")) {
          digitalWrite(pin1, HIGH);
          Serial.println("Ligou o pino 1");
          status1=1;
        }
        if (linhaatual.endsWith("GET /on2")) {
          digitalWrite(pin2, HIGH);
          Serial.println("Ligou o pino 2");
          status2=1;
        }
        if (linhaatual.endsWith("GET /on3")) {
          digitalWrite(pin3, HIGH);
          Serial.println("Ligou o pino 3");
          status3=1;
        }
        if (linhaatual.endsWith("GET /on4")) {
          digitalWrite(pin4, HIGH);
          Serial.println("Ligou o pino 4");
          status4=1;
        }
        if (linhaatual.endsWith("GET /on5")) {
          digitalWrite(pin5, HIGH);
          Serial.println("Ligou o pino 5");
          status5=1;
        }
        //----------------------------------------

        //Se for um pino começando com off desliga ele
        if (linhaatual.endsWith("GET /off1")) {
          digitalWrite(pin1, LOW);
          Serial.println("Desligou o pino 1");
          status1=0;
        }
        if (linhaatual.endsWith("GET /off2")) {
          digitalWrite(pin2, LOW);
          Serial.println("Desligou o pino 2");
          status2=0;          
        }
        if (linhaatual.endsWith("GET /off3")) {
          digitalWrite(pin3, LOW);
          Serial.println("Desligou o pino 3");
          status3=0;
        }
        if (linhaatual.endsWith("GET /off4")) {
          digitalWrite(pin4, LOW);
          Serial.println("Desligou o pino 4");
          status4=0;
        }
        if (linhaatual.endsWith("GET /off5")) {
          digitalWrite(pin5, LOW);
          Serial.println("Desligou o pino 5");
          status5=0;
        }
        //------------------------------------------------
      }
    }
    //Termina a conexão
    client.stop();
    Serial.println();
  }
  //loop de gerenciamento pela interface da oled
  //passa de menu lista para menu
  if(digitalRead(btn2) == 1){
    if((digitalRead(btn2) == 1) && (pagina=="menu_lista")){
      lista=0;
      pagina="menu";
      type="menu";
      y=64;
      delay(100);
      display.clear();
      display.drawXbm(posX, y, menu_width, menu_height, menu_bits); 
      display.display();
      y=48;
      delay(100);
      display.clear();
      display.drawXbm(posX, y, menu_width, menu_height, menu_bits); 
      display.display();
      y=32;
      delay(100);
      display.clear();
      display.drawXbm(posX, y, menu_width, menu_height, menu_bits); 
      display.display();
      y=16;
      delay(100);
      display.clear();
      display.drawXbm(posX, y, menu_width, menu_height, menu_bits); 
      display.display();
      y=0;
      delay(100);
      display.clear();
      display.drawXbm(posX, y, menu_width, menu_height, menu_bits); 
      display.display();
      //---------------------------------------
      
      //passa de menu para menu lista
    }else if((digitalRead(btn2) == 1) && (type=="menu")){
      menu=0;
      pagina="menu_lista";
      type="lista";
      y=0;
      delay(100);
      display.clear();
      display.drawXbm(posX, y, menu_lista_width, menu_lista_height, menu_lista_bits); 
      display.display();
      y=16;
      delay(100);
      display.clear();
      display.drawXbm(posX, y, menu_lista_width, menu_lista_height, menu_lista_bits); 
      display.display();
      y=32;
      delay(100);
      display.clear();
      display.drawXbm(posX, y, menu_lista_width, menu_lista_height, menu_lista_bits); 
      display.display();
      y=48;
      delay(100);
      display.clear();
      display.drawXbm(posX, y, menu_lista_width, menu_lista_height, menu_lista_bits); 
      display.display();
      y=64;
      delay(100);
      display.clear();
      display.drawXbm(posX, y, menu_lista_width, menu_lista_height, menu_lista_bits); 
      display.display();
      y=0;
      delay(100);
      display.clear();
      display.drawXbm(posX, y, menu_lista_width, menu_lista_height, menu_lista_bits); 
      display.display();
    }
    //------------------------------------

    //botão de seleção da tela da opção 0 para a opção 4 do menu lista
  }else if((digitalRead(btn1) == 1) && (type=="lista")){
    if(lista==0){
      pag();
      lista=1;
      display.drawXbm(posX, posY, lista1_width, lista1_height, lista1_bits); 
      display.display();
    }else if(lista==1){
      pag();
      lista=2;
      display.drawXbm(posX, posY, lista2_width, lista2_height, lista2_bits); 
      display.display();
    }else if(lista==2){
      pag();
      lista=3;
      display.drawXbm(posX, posY, lista3_width, lista3_height, lista3_bits); 
      display.display();
    }else if(lista==3){
      pag();
      lista=4;
      display.drawXbm(posX, posY, lista4_width, lista4_height, lista4_bits); 
      display.display();
    }
    //--------------------------------------

    //botão de seleção da tela da opção 4 para a opção 1 do menu lista
  }else if((digitalRead(btn4) == 1) && (type=="lista")){
    if(lista==4){
      pag();
      lista=3;
      display.drawXbm(posX, posY, lista3_width, lista3_height, lista3_bits); 
      display.display();
    }else if(lista==3){
       pag();
       lista=2;
       display.drawXbm(posX, posY, lista2_width, lista2_height, lista2_bits); 
       display.display();
    }else if(lista==2){
       pag();
       lista=1;
       display.drawXbm(posX, posY, lista1_width, lista1_height, lista1_bits); 
       display.display();
    }
    //----------------------------------------
    
    //botão de confirmação do menu lista
  }else if((digitalRead(btn3) == 1) && (type=="lista")){
    if(pagina=="menu_lista"){
      if((lista==1) && (digitalRead(btn3) == 1)){
       ligar_tudo();
       delay(100);
      }else if((lista==2) && (digitalRead(btn3) == 1)){
       desligar_tudo();
       delay(100);
      }else if((lista==3) && (digitalRead(btn3) == 1)){
       animacao=0;
       tentativa=0;
       WiFi.softAPdisconnect(true); //desconecta o servidor
       servidor=0;
       display.println("mudando para o wifi");
       Serial.println();
       wifi();
       delay(100);
      }else if((lista==4) && (digitalRead(btn3) == 1)){
        digitalWrite(reiniciar, LOW);
      }
    }
    //-------------------------------------

    //botão de seleção da tela da opção do pino 5 para a opção do pino 1 do menu
  }else if((digitalRead(btn1) == 1) && (pagina=="menu")){
    type="menu";
    if((menu==5) && (digitalRead(btn1) == 1)){
      menu=4;
      x=21;
      delay(100);
      display.clear();
      display.drawXbm(x, posY, pino4_width, pino4_height, pino4_bits); 
      display.display();
      x=42;
      delay(100);
      display.clear();
      display.drawXbm(x, posY, pino4_width, pino4_height, pino4_bits); 
      display.display();
      x=63;
      delay(100);
      display.clear();
      display.drawXbm(x, posY, pino4_width, pino4_height, pino4_bits); 
      display.display();
      x=84;
      delay(100);
      display.clear();
      display.drawXbm(x, posY, pino4_width, pino4_height, pino4_bits); 
      display.display();
      x=21;
      delay(100);
      display.clear();
      display.drawXbm(x, posY, pino4_width, pino4_height, pino4_bits); 
      display.display();
    }else if((menu==4) && (digitalRead(btn1) == 1)){
      menu=3;
        x=21;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino3_width, pino3_height, pino3_bits); 
        display.display();
        x=42;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino3_width, pino3_height, pino3_bits); 
        display.display();
        x=63;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino3_width, pino3_height, pino3_bits); 
        display.display();
        x=84;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino3_width, pino3_height, pino3_bits); 
        display.display();
        x=21;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino3_width, pino3_height, pino3_bits); 
        display.display();
    }else if((menu==3) && (digitalRead(btn1) == 1)){
      menu=2;
        x=21;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino2_width, pino2_height, pino2_bits); 
        display.display();
        x=42;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino2_width, pino2_height, pino2_bits); 
        display.display();
        x=63;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino2_width, pino2_height, pino2_bits); 
        display.display();
        x=84;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino2_width, pino2_height, pino2_bits); 
        display.display();
        x=21;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino2_width, pino2_height, pino2_bits); 
        display.display();
    }else if((menu==2) && (digitalRead(btn1) == 1)){
      menu=1;
        x=21;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino1_width, pino1_height, pino1_bits); 
        display.display();
        x=42;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino1_width, pino1_height, pino1_bits); 
        display.display();
        x=63;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino1_width, pino1_height, pino1_bits); 
        display.display();
        x=84;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino1_width, pino1_height, pino1_bits); 
        display.display();
        x=21;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino1_width, pino1_height, pino1_bits); 
        display.display();
    }
    //-----------------------------------------

    //botão de seleção da tela da opção do pino 5 para a opção do pino 1 menu
  }else if((digitalRead(btn4) == 1) && (type=="menu")){
    if((menu==0) && (digitalRead(btn4) == 1)){
      menu=1;
        x=21;
        display.clear();
        display.drawXbm(x, posY, pino1_width, pino1_height, pino1_bits); 
        display.display();
    }else if((menu==1) && (digitalRead(btn4) == 1)){
      menu=2;
        x=63;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino2_width, pino2_height, pino2_bits); 
        display.display();
        x=42;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino2_width, pino2_height, pino2_bits); 
        display.display();
        x=84;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino2_width, pino2_height, pino2_bits); 
        display.display();
        x=21;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino2_width, pino2_height, pino2_bits); 
        display.display();
    }else if((menu==2) && (digitalRead(btn4) == 1)){
      menu=3;
        x=63;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino3_width, pino3_height, pino3_bits); 
        display.display();
        x=42;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino3_width, pino3_height, pino3_bits); 
        display.display();
        x=84;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino3_width, pino3_height, pino3_bits); 
        display.display();
        x=21;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino3_width, pino3_height, pino3_bits); 
        display.display();
    }else if((menu==3) && (digitalRead(btn4) == 1)){
      menu=4;
        x=63;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino4_width, pino4_height, pino4_bits); 
        display.display();
        x=42;delay(100);
        display.clear();
        display.drawXbm(x, posY, pino4_width, pino4_height, pino4_bits); 
        display.display();
        x=84;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino4_width, pino4_height, pino4_bits); 
        display.display();
        x=21;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino4_width, pino4_height, pino4_bits); 
        display.display();
    }else if((menu==4) && (digitalRead(btn4) == 1)){
      menu=5;
        x=63;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino5_width, pino5_height, pino5_bits); 
        display.display();
        x=42;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino5_width, pino5_height, pino5_bits); 
        display.display();
        x=84;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino5_width, pino5_height, pino5_bits); 
        display.display();
        x=21;
        delay(100);
        display.clear();
        display.drawXbm(x, posY, pino5_width, pino5_height, pino5_bits); 
        display.display();
    }
    //------------------------------------------------------

    //botão de confirmação da tela do menu  
  }else if((digitalRead(btn3) == 1) && (pagina=="menu")){
    if(menu == 0){
      pagina="menu";
      pag();  
    }else if(menu==1){
      if((status1==0) && (digitalRead(btn3) == 1)){
        status1=1;
        digitalWrite(pin1, HIGH);
        Serial.println("Ligou o pino 1");
        delay(100);
      }else if((status1==1) && (digitalRead(btn3) == 1)){
        status1=0;
        digitalWrite(pin1, LOW);
        Serial.println("Desligou o pino 1");
        delay(100);
      }
      }else if(menu==2){
        if((status2==0) && (digitalRead(btn3) == 1)){
        status2=1;
        digitalWrite(pin2, HIGH);
        Serial.println("Ligou o pino 2");
        delay(100);
      }else if((status2==1) && (digitalRead(btn3) == 1)){
        status2=0;
        digitalWrite(pin2, LOW);
        Serial.println("Desligou o pino 2");
        delay(100);
      }      
    }else if(menu==3){
        if((status3==0) && (digitalRead(btn3) == 1)){
        status3=1;
        digitalWrite(pin3, HIGH);
        Serial.println("Ligou o pino 3");
        delay(100);
      }else if((status3==1) && (digitalRead(btn3) == 1)){
        status3=0;
        digitalWrite(pin3, LOW);
        Serial.println("Desligou o pino 3");
        delay(100);
      }
    }else if(menu==4){
        if((status4==0) && (digitalRead(btn3) == 1)){
        status4=1;
        digitalWrite(pin4, HIGH);
        Serial.println("Ligou o pino 4");
        delay(100);
      }else if((status4==1) && (digitalRead(btn3) == 1)){
        status4=0;
        digitalWrite(pin4, LOW);
        Serial.println("Desligou o pino 4");
        delay(100);
      }
    }else if(menu==5){
        if((status5==0) && (digitalRead(btn3) == 1)){
        status5=1;
        digitalWrite(pin5, HIGH);
        Serial.println("Ligou o pino 5");
        delay(100);
      }else if((status5==1) && (digitalRead(btn3) == 1)){
        status5=0;
        digitalWrite(pin5, LOW);
        Serial.println("Desligou o pino 5");
        delay(100);
      }
    }
    delay(100);
  }
  //---------------------------------------------  
}

void ligar_tudo(){ //função de ligar tudo
        digitalWrite(pin1, HIGH);
        digitalWrite(pin2, HIGH);
        digitalWrite(pin3, HIGH);
        digitalWrite(pin4, HIGH);
        digitalWrite(pin5, HIGH);
        status1=1;
        status2=1;
        status3=1;
        status4=1;
        status5=1;
        Serial.println("Ligou todos os pinos");
        delay(100);
        }

void desligar_tudo(){ //função de desligar tudo
        digitalWrite(pin1, LOW);    
        digitalWrite(pin2, LOW);
        digitalWrite(pin3, LOW);
        digitalWrite(pin4, LOW);
        digitalWrite(pin5, LOW);
        status1=0;
        status2=0;
        status3=0;
        status4=0;
        status5=0;
        Serial.println("Desligar todos os pinos");
        delay(100);
        }

void pag(){ //função de mudra de pagina
      if(pagina=="menu_lista"){
        display.clear();
        display.drawXbm(posX, posY, menu_lista_width, menu_lista_height, menu_lista_bits); 
        display.display();
      }else if(pagina=="menu"){
        display.clear();
        display.drawXbm(posX, posY, menu_width, menu_height, menu_bits); 
        display.display();
      }
}
